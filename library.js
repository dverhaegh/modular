
     /*
        Function Test, where var self holds all the functions.
    */
function Test(selector){
    var self = {};
    self.selector = selector;
    self.element = document.querySelector(self.selector);
     /*
        The function hide  put display style on hide to hide element
    */
    self.hide = function(){
        self.element.style.display = " none";
        return self;
    }

     /*
        The function show  put display style on block to show element
    */
    self.show = function(){
        self.element.style.display = " block";
        return self;
    }

     /*
        The function getRandomColor generates hexcode. Code picks randomly between 16 options, 6 times to create a hexcode.
    */
        function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
          color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
      }

     /*
        The function generator generates uses the function getRandomColor for the hexcode and sets style element color to random hexcode.
    */
    self.generator = function(){
        self.element.style.color = getRandomColor();
        return self;
    }
    
    return self;
}

